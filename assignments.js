const Config = require('electron-config')
const config = new Config();
const ipc = require('electron').ipcRenderer;

var commitCheck = 0;
var semester = ""
var course = ""
var project = ""
var projectCount = 0
var courseName = ""
var index = 1
var projectDate = ""
var sectionCount = 0
var courseArray = [];
var sectionArray = [];
var projectList = [];
var uniqueCourseList = [];
var uniqueSectionList = [];
var projectName = [];
var projectURL = [];

String.prototype.replaceAt=function(index, char) {
    var a = this.split("");
    a[index] = char;
    return a.join("");
}

function checkPage() {
  var fileName = location.href.split("/").slice(-1);
  if (fileName != "assignments.html") {
    return false
  }
}
function onLoad(){
  document.getElementById('profilename').innerHTML = config.get('name');
  load_projects()
  if (checkPage() == false) {
    addCourse()
    populateList()
  }
}

// Function to load and initialize the projects
function load_projects() {
  projects_response = ipc.sendSync('load_projects');

  document.getElementById("assignmentsCount").innerHTML = projects_response.length + " Assignments";

  for (var i = 0; i < projects_response.length; i++) {
    semester = projects_response[i].split_namespace_result.semester
    course = projects_response[i].split_namespace_result.course
    project = projects_response[i].split_namespace_result.project
    courseName = projects_response[i].split_namespace_result.courseName
    projectDate = String(projects_response[i].created_at).substring(0,10)
    section = projects_response[i].split_namespace_result.section
    projectName[i] = projects_response[i].project_name
    projectURL[i] = projects_response[i].ssh_url

    for (var x = 0; x < courseName.length; x++)
    {
        var c = courseName.charAt(x);
        if (c == '_') {
          courseName = courseName.replaceAt(x, " ")
        }
    }

    courseArray.push(courseName);

    if (checkPage() != false) {
      insertRowAssignments(courseName, project, "Pending");
    } else {
      insertRowSections(courseName, section)
    }

    projectList.push([projectDate, course, courseName, section, project, "Pending"]);
  }

  for (i = 0; i < projectList.length; i++) {
      sectionArray.push(projectList[i][3])
  }

// Filter redundant courses into a unique list of courses
  uniqueCourseList = courseArray.filter(function(elem, index, self) {
    return index == self.indexOf(elem);
  })
}

// Insert rows of current assignments into table in assignments.html
function insertRowAssignments(courseName, project, status){
          var table=document.getElementById("assignmentsSummary");
          var row=table.insertRow(table.rows.length);

          var cell1=row.insertCell(0);
          var t1=document.createElement("null");
              t1.id = "courseName"+index;
              t1.innerHTML = courseName;
              cell1.appendChild(t1);

          var cell2=row.insertCell(1);
          var t2=document.createElement("null");
              t2.id = "section"+index;
              t2.innerHTML = section;
              cell2.appendChild(t2);

          var cell3=row.insertCell(2);
          var t3=document.createElement("null");
              t3.id = "project"+index;
              t3.innerHTML = project;
              cell3.appendChild(t3);

          var cell4=row.insertCell(3);
          var t4=document.createElement("null");
              t4.id = "status"+index;
              t4.innerHTML = status;
              cell4.appendChild(t4);
    index++;
}

// Insert rows of current assignments into table in add-new-assignment.html
function insertRowSections(courseName, section){
          var table=document.getElementById("sectionTable");
          var row=table.insertRow(table.rows.length);

          var cell1=row.insertCell(0);
          var t1=document.createElement("input");
              t1.setAttribute("type", "checkbox");
              t1.id = "checkbox"+index;
              t1.value = "sectioncheck "+index;
              t1.innerHTML = section;
              cell1.appendChild(t1);

          var cell2=row.insertCell(1);
          var t2=document.createElement("null");
              t2.setAttribute("text-align","left")
              t2.id = "section"+index;
              t2.innerHTML = courseName + ": Section " + section;
              cell2.appendChild(t2);

          var cell3=row.insertCell(2);
          var t3=document.createElement("null");
              t3.id = "empty"+index;
              cell3.appendChild(t2);
    index++;
    console.log(t1,t2,t3)
}

// Function to add a course to the drop-down list in add-new-assignment.html
function addCourse() {
  for (i = 0; i < uniqueCourseList.length; i++) {
    var crs = document.getElementById("courseSelect");
    var option = document.createElement("option");
      option.text = uniqueCourseList[i]
      option.value = uniqueCourseList[i]
      crs.add(option);
  }
}

// Function to filter the list of sections based on the selected course
function filterSectionList(courseName) {

  // var filter = document.getElementById('sectionFilter')
  // while(filter.options.length > 1) {
  //     filter.remove(filter.options.length - 1)
  // }

  for (i = 0; i < projectList.length; i++) {
    if (courseName == projectList[i][2]) {
      sectionArray.push(projectList[i][3])
    }
  }

  uniqueSectionList = sectionArray.filter(function(elem, index, self) {
    return index == self.indexOf(elem);
  })

  uniqueSectionList.sort()
  console.log(uniqueSectionList)

  // for (i = 0; i < uniqueSectionList.length; i++) {
  //   var stn = document.getElementById("sectionFilter");
  //   var option = document.createElement("option");
  //     option.text = uniqueSectionList[i]
  //     option.value = uniqueSectionList[i]
  //     stn.add(option);
  // }
}

// Function to populate the table with the list of sections, according to the course
function populateList() {

  $('select').on('change', function () {
    var table = document.getElementById('sectionTable')
    while(table.rows.length > 1) {
        table.deleteRow(1);
    }

    var courseList = document.getElementById('courseSelect')
    // var sectionsList = document.getElementById('sectionFilter')


    filterSectionList(courseList.options[courseList.selectedIndex].value)

    if (courseList.options[courseList.selectedIndex].value == 'SelectCourse') {
      // for (i = 0; i < projectList.length; i++) {
      //   insertRowSections(projectList[i][3])
      //   console.log(projectList[i])
      while(table.rows.length > 1) {
          table.deleteRow(1);
      }

      for (i = 0; i < projectList.length; i++) {
        if (courseList.options[courseList.selectedIndex].value == projectList[i][2]) {
          // insertRowSections(projectList[i][3])
          // if (sectionsList.options[sectionsList.selectedIndex].value == 'All Sections') {
          //   insertRow(projectList[i][0],projectList[i][1],projectList[i][2],projectList[i][3],projectList[i][4],projectList[i][5])
          //   console.log(projectList[i])
          // } else {
          //   if (sectionsList.options[sectionsList.selectedIndex].value == projectList[i][3]) {
          //     insertRow(projectList[i][0],projectList[i][1],projectList[i][2],projectList[i][3],projectList[i][4],projectList[i][5])
          //     console.log(projectList[i])
          // }
        }
      }
    }
  });
}

function cloneAllAssignments(){
  var errorCount = 0;
    folderDestination = config.get('downloadPath');
    console.log("Destination Path", folderDestination)
    for(i = 0; i < projectName.length; i++) {
      console.log("URL: ", projectURL[i])
      console.log("projectName: ", projectName[i])
      var clone_response = ipc.sendSync('clone_project', projectURL[i], folderDestination, projectName[i]);
      if (clone_response.response) {
        currentRepo = config.get('current_local_repo')
        // $('#localRepo').html(slice_localRepo_name(currentRepo))
        // assignmentSelected = false
        // populate_open_repo()
        alert("Successfully cloned the repository")
      }
      else {
        errorCount++;
      }
    }
    if(errorCount > 0) {
      alert(clone_response.message)
    }
}

function commitButton(){
  var courseList = document.getElementById('courseSelect');
  if (courseList.selectedIndex > 0) {
    var selectedCourse = courseList.options[courseList.selectedIndex].text;
    if (newAssignmentName.value) {
      var assignmentName = document.getElementById('newAssignmentName').value;
      var sectionList = $('input:checkbox:checked').map(function() {
        return this.value;
      }).get();
      if (!sectionList.value) {
        if (commitMessage.value) {
          alert("An new assignment " + assignmentName + " for " + selectedCourse + " was committed to " + sectionList + " with commit message: " + commitMessage.value);
          commitCheck = 1;
        } else {
          alert("Please enter a valid commit message")
        }
      } else {
        alert("Please select the sections to assign to.")
    }
  }else {
      alert("Please enter the assignment name.")
    }
  } else {
    alert("Please select a course first!")
  }
}

function pushButton(){
  if (commitCheck == 1) {
    var courseList = document.getElementById('courseSelect');
    var selectedCourse = courseList.options[courseList.selectedIndex].text;
    var assignmentName = document.getElementById('newAssignmentName').value;
    var sectionList = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();
    alert("The assignment " + assignmentName + " for " + selectedCourse + " was pushed to " + sectionList);
  } else {
    alert("Please commit first.")
  }
}

function logout() {
  document.href = "login.html";
  config.set('loggedIn', false);
  config.set('username',"");
  config.set('profilename',"");
  config.set('private_token',"");
}
