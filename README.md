# GitSubmit Instructor UI

##Synopsis
The project aims to deliver a graphical user interface which Instructors and Teaching/Grading Assistants can use to interact with assignments and grades with respect to students' submissions. The client itself is based on Git, and will be cross-platform compatible.

## About this application
This application uses the Electron framework to provide cross-platform compatibility.

An application written using Electron usually contains three particular files:

`package.json` - This .json file points to the application's main file and lists all its details and dependencies.

`main.js` - This .json file starts the application and creates a Chromium browser instance to render HTML. This is the application's main process.

`index.html` - A web page to be rendered. Your application's view will be written in this file. This is the applications's renderer process.

##License
This project operates under the Apache 2.0 (Apache-2.0). You can read more under [`LICENSE.md`](https://bitbucket.org/gitsubmitsuite/gitsubmit_instructor/src/f1bfbccfc9edcf11fad85be863b0ec280c21d142/LICENSE.md?at=master&fileviewer=file-view-default)