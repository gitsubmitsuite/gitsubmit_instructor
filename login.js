const ipc = require('electron').ipcRenderer;
const Config = require('electron-config');
const config = new Config();

var login_success;

function login() {
  // Validate login credentials
  var username = document.getElementById("username").value;
  var password  = document.getElementById("password").value;

  if ( username != "" && password != ""){
    login_success = ipc.sendSync('login', username,password);
    if (login_success){
      alert ("Login success!\nHello, " + config.get('name') + "!");
      ipc.send('show-overview');
    } else {
      alert ("Incorrect Usename or Password");
      event.preventDefault();
    }
  }
  else{
    alert("Username and Password can not be empty")
  }



  config.set('loggedIn',true);
  config.set('username',username);
}

// Method to remember the user
function rememberMe() {
 var rememberme = document.getElementById('remember-me');
 if (rememberme.checked == true) {
   // Code to maintain the session
 } else {
   // Ignore and leave the function gracefully
 }
 config.set('loggedIn', true);
}

//reset button functionality
function reset(){
  document.getElementById("username").value = "";
  document.getElementById("password").value = "";
}

function logout() {
  config.set('loggedIn', false)
  config.set('username', "");
}
