const Config = require('electron-config');
const config = new Config();
const ipc = require('electron').ipcRenderer

const selectDirBtn = document.getElementById('select-directory')

selectDirBtn.addEventListener('click', function (event) {
  ipc.send('open-file-dialog')
})

ipc.on('selected-directory', function (event, path) {
  document.getElementById('selected-file').innerHTML = path
  config.set('downloadPath',path)
})
function onLoad() {
  console.log(config.get('username'));
  console.log(config.get('downloadPath'));
  document.getElementById("profilename").innerHTML = config.get('name');
  document.getElementById("settingsUsername").innerHTML = config.get('name');
  document.getElementById("settingsMailid").innerHTML = config.get('username');
}

function logout() {
  document.href = "login.html";
  config.set('loggedIn', false);
  config.set('username',"");
  config.set('profilename',"");
  config.set('private_token',"");
}

function popupFunction() {
    var popup = document.getElementById("helpPopup");
    popup.classList.toggle("show");
}

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      function updatePassword() {

        var password1,password2, match, currentPassword;
    password1 = document.getElementById("password1").value;
    password2 = document.getElementById("password2").value;
    currentPassword = document.getElementById("currentPassword").value;
    // match = (password1==password2) ? alert("Password was updated successfully!"):alert("Password are not same");
    if(password1=="" || password2=="" || currentPassword==""){
      alert("Please fill the password field and confirm it");
      document.getElementById("password1").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
      document.getElementById("password2").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
    }else if(password1!=password2) {
      alert("Passwords are not same");
      document.getElementById("password1").style.border = "2px solid red";
      document.getElementById("password2").style.border = "2px solid red";
    }else if((password1==password2)&&(password1==currentPassword)) {
      alert("New password must not be current password");
      document.getElementById("password1").value = "";
      document.getElementById("password2").value = "";
      document.getElementById("currentPassword").value = "";
      document.getElementById("password1").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
      document.getElementById("password2").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
    }else if((String(password1).split("").length<8)||(String(password2).split("").length<8)) {
      alert("Check password criteria password must have atleast 8 characters");
      document.getElementById("password1").value = "";
      document.getElementById("password2").value = "";
      document.getElementById("currentPassword").value = "";
      document.getElementById("password1").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
      document.getElementById("password2").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
    }else if((password1==password2)&&(password1!=currentPassword)){
    alert("Password was updated successfully!");
    document.getElementById("password1").value = "";
    document.getElementById("password2").value = "";
    document.getElementById("currentPassword").value = "";
    document.getElementById("password1").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
    document.getElementById("password2").style = "border-radius:10px;border-size:1px;padding-left:5px;padding-right:5px;";
    }
      }

      function changeLocation() {
         document.getElementById('changeLocation').click();

        // alert("Location was changed successfully!")
      }
//       function selectFolder(e) {
//   document.getElementById('location').value = "superdirectory/maindirectory/subdirectory"
// }
