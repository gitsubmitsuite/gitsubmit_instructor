const Config = require('electron-config');
const config = new Config();
const ipc = require('electron').ipcRenderer;

var semester = ""
var course = ""
var project = ""
var projectCount = 0
var courseName = ""
var index = 1
var projectDate = ""
var sectionCount = 0
var courseArray = [];
var sectionArray = [];
var projectList = [];
var uniqueCourseList = [];
var uniqueSectionList = [];

String.prototype.replaceAt=function(index, char) {
    var a = this.split("");
    a[index] = char;
    return a.join("");
}

function load_projects() {
  projects_response = ipc.sendSync('load_projects');

  document.getElementById("countOfAssignments").innerHTML = projects_response.length;

  for (var i = 0; i < projects_response.length; i++) {
    semester = projects_response[i].split_namespace_result.semester
    course = projects_response[i].split_namespace_result.course
    project = projects_response[i].split_namespace_result.project
    courseName = projects_response[i].split_namespace_result.courseName
    projectDate = String(projects_response[i].created_at).substring(0,10)
    section = projects_response[i].split_namespace_result.section

    for (var x = 0; x < courseName.length; x++)
    {
        var c = courseName.charAt(x);
        if (c == '_') {
          courseName = courseName.replaceAt(x, " ")
        }
    }

    courseArray.push(courseName);

    insertRow(projectDate, course, courseName, section, project, "Pending");
    projectList.push([projectDate, course, courseName, section, project, "Pending"]);
  }

  for (i = 0; i < projectList.length; i++) {
      sectionArray.push(projectList[i][3])
  }
  console.log("Section Array: ", sectionArray)
  document.getElementById("countOfSections").innerHTML = sectionArray.length;

  uniqueCourseList = courseArray.filter(function(elem, index, self) {
    return index == self.indexOf(elem);
  })

}

function populateTable() {

  $('select').on('change', function () {
    var table = document.getElementById('summaryTable')
    while(table.rows.length > 1) {
        table.deleteRow(1);
    }

    var courseList = document.getElementById('courseFilter')
    var sectionsList = document.getElementById('sectionFilter')

    filterSectionList(courseList.options[courseList.selectedIndex].value)

    if (courseList.options[courseList.selectedIndex].value == 'All Courses') {
      for (i = 0; i < projectList.length; i++) {
        insertRow(projectList[i][0],projectList[i][1],projectList[i][2],projectList[i][3],projectList[i][4],projectList[i][5])
        console.log(projectList[i])
      }
    } else {
      for (i = 0; i < projectList.length; i++) {
        if (courseList.options[courseList.selectedIndex].value == projectList[i][2]) {

          if (sectionsList.options[sectionsList.selectedIndex].value == 'All Sections') {
            insertRow(projectList[i][0],projectList[i][1],projectList[i][2],projectList[i][3],projectList[i][4],projectList[i][5])
            console.log(projectList[i])
          } else {
            if (sectionsList.options[sectionsList.selectedIndex].value == projectList[i][3]) {
              insertRow(projectList[i][0],projectList[i][1],projectList[i][2],projectList[i][3],projectList[i][4],projectList[i][5])
              console.log(projectList[i])
          }
        }

        }
      }
    }
  });
}

function insertRow(date, course, courseName, section, projectName, status){
          var table=document.getElementById("summaryTable");
          var row=table.insertRow(table.rows.length);

          var cell1=row.insertCell(0);
          var t1=document.createElement("null");
              t1.id = "date"+index;
              t1.innerHTML = date;
              cell1.appendChild(t1);

          var cell2=row.insertCell(1);
          var t2=document.createElement("null");
              t2.id = "coursenumber"+index;
              t2.innerHTML = course;
              cell2.appendChild(t2);

          var cell3=row.insertCell(2);
          var t3=document.createElement("null");
              t3.id = "coursename"+index;
              t3.innerHTML = courseName;
              cell3.appendChild(t3);

          var cell4=row.insertCell(3);
          var t4=document.createElement("null");
              t4.id = "section"+index;
              t4.innerHTML = section;
              cell4.appendChild(t4);

          var cell5=row.insertCell(4);
          var t5=document.createElement("null");
              t5.id = "name"+index;
              t5.innerHTML = projectName;
              cell5.appendChild(t5);

          var cell6=row.insertCell(5);
          var t6=document.createElement("null");
              t6.id = "status"+index;
              t6.innerHTML = status;
              cell6.appendChild(t6);
    index++;
}

function addCourse() {
  for (i = 0; i < uniqueCourseList.length; i++) {
    var crs = document.getElementById("courseFilter");
    var option = document.createElement("option");
      option.text = uniqueCourseList[i]
      option.value = uniqueCourseList[i]
      crs.add(option);
  }
}

function filterSectionList(courseName) {

  var filter = document.getElementById('sectionFilter')
  while(filter.options.length > 1) {
      filter.remove(filter.options.length - 1)
  }

  for (i = 0; i < projectList.length; i++) {
    if (courseName == projectList[i][2]) {
      sectionArray.push(projectList[i][3])
    }
  }

  uniqueSectionList = sectionArray.filter(function(elem, index, self) {
    return index == self.indexOf(elem);
  })

  uniqueSectionList.sort()
  console.log(uniqueSectionList)

  for (i = 0; i < uniqueSectionList.length; i++) {
    var stn = document.getElementById("sectionFilter");
    var option = document.createElement("option");
      option.text = uniqueSectionList[i]
      option.value = uniqueSectionList[i]
      stn.add(option);
  }
}

function searchSummary() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.getElementById("summarySearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("summaryTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function onLoad() {
  document.getElementById("profilename").innerHTML = config.get('name');
  var crs = document.getElementById("courseFilter");
  var option = document.createElement("option");
  load_projects();
  console.log(courseArray);
  console.log(projectList);
  addCourse();
  populateTable();
  //console.log(os.homedir())
}

function logout() {
  document.href = "login.html";
  config.set('loggedIn', false);
  config.set('username',"");
  config.set('profilename',"");
  config.set('private_token',"");
}
