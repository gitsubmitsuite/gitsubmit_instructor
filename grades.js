    const Config = require('electron-config');
    const config = new Config();
    const ipc = require('electron').ipcRenderer;

    // var projects_response =  ipc.sendSync('load_projects');
    var semester = ""
    var course = ""
    var project = ""
    var section = ""
    var courseName = ""
    var index = 1
    var section = ""
    var sectionName = "";
    var courseArray = [];
    var assignmentArray = [];
    var uniqueCourseList = [];
    var uniqueAssignmentList = [];
    var downloadPath = config.get('downloadPath')

    String.prototype.replaceAt=function(index, char) {
        var a = this.split("");
        a[index] = char;
        return a.join("");
    }

    function onLoad() {
      document.getElementById("profilename").innerHTML = config.get('name');
      load_projects();
      addCourse();
    }

    function load_projects() {
      projects_response = ipc.sendSync('load_projects');
      console.log("project response in loding projects:",projects_response);
      for (var i = 0; i < projects_response.length; i++) {
        semester = projects_response[i].split_namespace_result.semester
        course = projects_response[i].split_namespace_result.course
        project = projects_response[i].split_namespace_result.project
        courseName = projects_response[i].split_namespace_result.courseName
        projectDate = String(projects_response[i].created_at).substring(0,10)
        section = projects_response[i].split_namespace_result.section
        sectionName =  section + " - " + courseName;

        for (var x = 0; x < courseName.length; x++)
        {
            var c = courseName.charAt(x);
            if (c == '_') {
              courseName = courseName.replaceAt(x, " ")
            }
        }

        courseArray.push(courseName);
        console.log("Section: ", section, "CourseName: ", courseName);
        insertRow(sectionName);
        projectList.push([projectDate, course, courseName, section, project, "Pending"]);
        insertAssignment(project);
      }

      for (i = 0; i < projectList.length; i++) {
          sectionArray.push(projectList[i][3])
      }

      uniqueCourseList = courseArray.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
      })
    }

    function populateAssignmentList() {
      // $('select').on('change', function () {
      //   var table = document.getElementById('summaryTable')
      //   while(table.rows.length > 1) {
      //       table.deleteRow(1);
      //   }
      // }
      var courseList = document.getElementById('courseSelect')

    }

    function insertRow(section){
              var table=document.getElementById("gradesSections");
              var row=table.insertRow(table.rows.length);
              var cell1=row.insertCell(0);
              var t1=document.createElement("input");
                  t1.setAttribute("type", "checkbox");
                  cell1.appendChild(t1);

              var cell2=row.insertCell(1);
              var t2=document.createElement("null");
                  t2.id = "section"+index;
                  t2.innerHTML = section;
                  cell2.appendChild(t2);

                  index++;
    }

    function addCourse() {
      for (i = 0; i < uniqueCourseList.length; i++) {
        var crs = document.getElementById("courseSelect");
        var option = document.createElement("option");
          option.text = uniqueCourseList[i]
          option.value = uniqueCourseList[i]
          crs.add(option);
      }
    }

    function insertAssignment(project){
      var prjt = document.getElementById("assignmentSelect");
      var option = document.createElement("option");
      option.text = project;
      prjt.add(option);
    }

    function updateButton(){
      alert("updateButton was clicked");
    }

    function selectAllSections(chk){
      for (i = 0; i < chk.length; i++)
      chk[i].checked = true ;
      alert("Select All Sections was clicked");
    }

    function deselectAllSections(chk){
      for (i = 0; i < chk.length; i++)
      chk[i].checked = false ;
      alert("Deselect All Sections was clicked");
    }

    function selectAllStudents(chk){
      for (i = 0; i < chk.length; i++)
      chk[i].checked = true ;
      alert("Select All Students was clicked");
    }

    function deselectAllStudents(chk){
      for (i = 0; i < chk.length; i++)
      chk[i].checked = false ;
      alert("Deselect All Students was clicked");
    }

    function commitButton(){
      var x = document.getElementById("assignment").selectedIndex;
      alert(document.getElementsByTagName("option")[x].value + " was commited");
    }

    function pushButton(){
      var x = document.getElementById("assignment").selectedIndex;
      alert(document.getElementsByTagName("option")[x].value + " was pushed");
    }

    function cloneButton(){
      alert("Clone Repositories was clicked");
      var destination = config.get('downloadPath');
      console.log("downloadPath for cloning:", destination[0]);
      folderDestination = destination[0];
      for(var i = 0; i < projects_response.length; i++){
        var repoUrlArray = [];
        repoUrlArray.push("git@csgrad06.nwmissouri.edu:"+semester+"-"+section+"."+sectionName+"/"+assignment+".git")
        console.log("repo url:",repoUrlArray);
      }
    }

    function pullButton(){
      alert("Pull All Repositories was clicked");
      console.log("downloadPath: ", config.get('downloadPath'));
      //alert(projects_response);
    //  alert("URL for cloning is : "+"http://csgrad06.nwmissouri.edu/"+username+"/"+projects_response+".git" );
      repoUrl = 'git@csgrad06.nwmissouri.edu:S2017-44555.NS/Grades_Test.git'
      //folderDestination = document.getElementById("---").value()
      var destination = config.get('downloadPath')
      console.log(destination[0]);
      folderDestination = destination[0];
      var clone_response = ipc.sendSync('clone_project', repoUrl, folderDestination,'Grades_Test');
            //alert(clone_response);
        	  //projects_response = ipc.sendSync('load_projects');

         		console.log(projects_response)

            for(var i = 0; i < projects_response.length; i++){
            semester = projects_response[i].split_namespace_result.semester
            course = projects_response[i].split_namespace_result.course
            project = projects_response[i].split_namespace_result.project
            section = projects_response[i].split_namespace_result.section
            courseName = projects_response[i].split_namespace_result.courseName

            console.log(semester, course, section, courseName, project)
            //document.getElementById("selectAssignment").append("<option>"+projects_response+"</option>");
          }

      if (clone_response) {
        alert("Clone success")
      }
      else {
        alert("Clone is Unsuccessful")
      }
  }


  function logout() {
    document.href = "login.html";
    config.set('loggedIn', false);
    config.set('username',"");
    config.set('profilename',"");
    config.set('private_token',"");
  }
