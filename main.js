    const electron = require('electron')
    // Module to control application life.
    const app = electron.app
    // Module to create native browser window.
    const BrowserWindow = electron.BrowserWindow

    // Module to handle configuration for Electron
    const Config = require('electron-config');
    const config = new Config();
    const request = require('request');
    const path = require('path')
    const url = require('url')
    const clone = require('./gitsubmit_backend/Libraries/clone')
    const auth = require('./gitsubmit_backend/Libraries/auth')
    const list_projects = require('./gitsubmit_backend/Libraries/list-projects')
    const create_ssh_keys = require('./gitsubmit_backend/Libraries/create_ssh_keys')
    const status = require('./gitsubmit_backend/Libraries/status')
    const add = require('./gitsubmit_backend/Libraries/add')
    const commit = require('./gitsubmit_backend/Libraries/commit')
    const log = require('./gitsubmit_backend/Libraries/log')
    const push = require('./gitsubmit_backend/Libraries/push')
    const pull = require('./gitsubmit_backend/Libraries/pull')
    const open_repo = require('./gitsubmit_backend/Libraries/open_repo')
    const Git = require('nodegit')
    const userDataPath = app.getPath('userData')
    const ipc = require('electron').ipcMain
    const dialog = require('electron').dialog
    const fc = require('extfs')

    var oid;
    var indexHead;
    var indexParent;
    var allRepos = [];
    var allLocalRepos = [];
    var downloadPath = config.get('downloadPath')
    var mainWindow

    // Create the browser window.
    function createWindow () {
      mainWindow = new BrowserWindow({
          width: 800,
          height: 600,
        center: true,
        fullscreen: false,
        nodeIntegration: false,
        devTools: false
      })

      if(config.get('loggedIn'))
      {
        mainWindow.loadURL(url.format({
          pathname: path.join(__dirname, 'overview.html'),
          protocol: 'file:',
          slashes: true
        }))
      }
      else{
        console.log("In main.js-- User is not loggedIn");
        mainWindow.loadURL(url.format({
          pathname: path.join(__dirname, 'login.html'),
          protocol: 'file:',
          slashes: true
        }))
      }
  // Exxecuting to print the app configuration file path
    // console.log(userDataPath)
      // Emitted when the window is closed.
      mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
      })
    }

    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    app.on('ready', createWindow)

    // Quit when all windows are closed.
    app.on('window-all-closed', function () {
      // On OS X it is common for applications and their menu bar
      // to stay active until the user quits explicitly with Cmd + Q
      if (process.platform !== 'darwin') {
        app.quit()
      }
    })

    app.on('activate', function () {
      // On OS X it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (mainWindow === null) {
        createWindow()
      }
    })

    // For selecting directory(folder) path
      ipc.on('open-file-dialog', function (event) {
      dialog.showOpenDialog({
        properties: ['openFile', 'openDirectory']
      }, function (files) {
        if (files) event.sender.send('selected-directory', files)
      })
    })

      ipc.on('login', function (event, user, pass) {
      // Authentication request
      //Async method call for request
      console.log(app.getPath('userData'));
      auth(user, pass, function (login_response) {
      console.log("Login success " + login_response)
      event.returnValue = login_response;
      });
    })

    ipc.on('get_current_repo', function (event) {
      console.log("GET CURRENT REPO" + config.get('current_local_repo'))
      event.returnValue = config.get('current_local_repo')
    })

    ipc.on('show-overview', function () {
     mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'overview.html'),
        protocol: 'file:',
        slashes: true
      }))
    })

    ipc.on('load_projects', function (event) {
      //console.log("Called load_projects")
      list_projects(function (returnedProjects) {
        //console.log("In main.js: Length of projects list==" + returnedProjects.length)
        if (returnedProjects[0].list_project_error) {
          event.returnValue = false
        }
        else {
          event.returnValue = returnedProjects
        }
      });
    })

// Function to clone existing projects from GitLab
  ipc.on('clone_project', function (event, repoUrl, folderDestination, project_name) {
    clone(repoUrl, folderDestination, project_name, function (response, messageObj) {
      //console.log("In main.js" + messageObj.message)
      if (response) {
        currentLocalRepo = folderDestination
        if (allRepos.indexOf(folderDestination) == -1) {
          allRepos.push({ 'localRepo': folderDestination + "\\" + project_name, 'remoteRepo': repoUrl })
          allLocalRepos.push({ folderDestination })
          config.set('allRepos', allRepos)
        }
        config.set('current_remote_repo', repoUrl)
        config.set('current_local_repo', path.join(folderDestination + "\\" + project_name))

      }
      //console.log("Sending the clone response")
      mainWindow.webContents.send('open_repo_list_update', allLocalRepos, allRepos)
      event.returnValue = { response: response, message: messageObj.message, empty: messageObj.empty }
    });

  })

// Function to pull all changes to local repo
ipc.on('pull_changes', function (event) {

  var repo = config.get('current_local_repo')
  var pullUrl = config.get('current_remote_repo')
  //console.log("Pull Changes in main in:" + repo + " from :" + pullUrl)
  pull(pullUrl, repo, function (response, messageObj) {
    //console.log("Came back with response:" + response + " with messageObj.message:" + messageObj.message)
    if (response) {
      //console.log("Under if after receiving the response from pull function")
      //console.log("In pull main with response:" + reponse + " and message is :" + messageObj.message)
      event.returnValue = { 'response': response, 'message': messageObj.message };
    }
    else {
      //console.log("In pull main with response:" + reponse + " and message is :" + messageObj.message)
      event.returnValue = { 'response': response, 'message': messageObj.message };
    }

  })

})

  ipc.on('clone_path', function (event, path) {
    console.log("Path from UI" + path)
    formatDirectory(path)
  })

  //Stage files in assignments tab using this function
  ipc.on('stage_files', function (event, filesToBeStaged) {
    //console.log('In main.js add files message:' + filesToBeStaged)

    add(filesToBeStaged, config.get('current_local_repo'), function (response, stageObject) {
      // console.log("In main.js - add result:" + response + " and oid:" + stageObject.oid + " and head: " + stageObject.indexHead + " and parent:" + stageObject.indexParent)
      if (response) {
        oid = stageObject.oid
      }
      event.returnValue = response;
    })
  })

//Commit changes to the server
  ipc.on('commit_changes', function (event, summary, description) {
    console.log(summary, description)
    var message = summary + "\n\n" + description
    var logObjectForUI;
    var commit_response;
    var commit_object;
    commit(config.get('current_local_repo'), oid, message, function (response, commitObj) {
      commit_response = response;
      commit_object = commitObj;
      console.log("Response for commit:" + response + " with commitId:" + commitObj.commitId)
      if (response) {
        log(function (response, logObject) {

          if (response) {
            logObjectForUI = logObject
          }
          console.log("***********In log")
          console.log("Log object is:" + logObjectForUI)
          send_commit_response()
        });
        function send_commit_response() {
          console.log("Out of log")
          event.returnValue = { 'response': response, 'commitId': commitObj.commitId, 'logObj': logObjectForUI };
        }
      }
      else {
        event.returnValue = { 'response': response, 'commitId': '', 'logObj': '' };
      }
    })
  })

// Set the default path based on the operating system
  ipc.on('set_default_path', function (event, path) {
    var shell = os.homedir()
    var system = window.navigator.platform
    if (system == "MacIntel") {
      console.log("The operating system is MacOS")
      path = "C:\\GitSubmit Suite\\Projects\\"
      config.set("downloadPath", path)
    } else {
      console.log("The operating system is Windows")
      path = "C:\\GitSubmit Suite\\Projects\\"
      config.set("downloadPath", path)
    }
  })

  ipc.on('check-status', function (event) {
    checkStatusForCurrentRepo()
  });

  // In this file you can include the rest of your app's specific main process
  // code. You can also put them in separate files and require them here.
