const Application = require('spectron').Application;
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = require('chai').expect;
const assert = require('chai').assert;
const path = require('path');

const electronPath = path.join(__dirname, '..', 'node_modules', '.bin', 'electron');
const appPath = path.join(__dirname, '..');

global.before(function () {
    chai.should();
    chai.use(chaiAsPromised);
});
describe('App starts and has correct title and buttons', function () {
  let app;

  before(function () {
      app = new Application({
        path: electronPath,
        args: [appPath]
      });
      return app.start();
  });

  it('opens a window', function () {
      return app.client.waitUntilWindowLoaded().getWindowCount()
              .should.eventually.equal(1);
    });

    it('tests the title', function () {
      return app.client.waitUntilWindowLoaded().getTitle()
              .should.eventually.equal('GitSubmit Suite - Login');
    });

    it('tests the Open File button text exists', function() {
      return app.client.getText('#sidebar')
              .should.eventually.equal('Overview\nSections\nAssignments\nGrades\nSettings');
    });
    // for sections page all the buttons are tested
    //Other elements in page are not tested
    it('tests the Open File button text exists', function() {
      return app.client.getText('#sectionsCount')
              .should.eventually.equal('6 Sections');
    });
    it('tests the Open File button text exists', function() {
      return app.client.getText('#sectionsCount')
              .should.eventually.equal('Add A New Section');
    });
    it('tests the Open File button text exists', function() {
      return app.client.getText('#sectionsCount')
              .should.eventually.equal('Modify A Section');
    });

    //for assignments page all the buttons are tested
    //Other elements in page are not tested
    it('tests the Open File button text exists', function() {
      return app.client.getText('#assignmentsCount')
              .should.eventually.equal('11 assignments');
    });
    it('tests the Open File button text exists', function() {
      return app.client.getText('#sectionsCount')
              .should.eventually.equal('Add A New assignment');
    });
    it('tests the Open File button text exists', function() {
      return app.client.getText('#sectionsCount')
              .should.eventually.equal('Modify A assignment');
    });

  after(function (done) {
      done();
      return app.stop();
  });

});
