const Config = require('electron-config')
const config = new Config();
const ipc = require('electron').ipcRenderer;

var project = ""
var courseName = ""
var section = ""
var index = 1

function onLoad(){
  document.getElementById("profilename").innerHTML = config.get('name');
  var rowCount = document.getElementById('sectionsSummary').rows.length;
  document.getElementById('sectionsCount').innerHTML = (rowCount-1) + " Sections";
}

function load_projects() {
  projects_response = ipc.sendSync('load_projects');

  document.getElementById("countOfAssignments").innerHTML = projects_response.length;

  for (var i = 0; i < projects_response.length; i++) {
    courseName = projects_response[i].split_namespace_result.courseName
    section = projects_response[i].split_namespace_result.section
    insertRow(courseName, section);
  }
}

function insertRow(courseName, section){
  var table=document.getElementById("sectionsSummary");
  var row=table.insertRow(table.rows.length);
  var cell1=row.insertCell(0);
  var t1=document.createElement("null");
  t1.id = "courseName-section"+index;
  t1.innerHTML = courseName + " - " + section;
  cell1.appendChild(t1);
  index++;
}

function overallView(){
  var rowCount = document.getElementById('sectionsSummary').rows.length;
  document.getElementById('sectionsCount').innerHTML = (rowCount-1) + " Sections"
}

function highlight(){
var table = document.getElementById('dataTable');
for (var i=0;i < table.rows.length;i++){
    table.rows[i].onclick= function () {
      if(!this.hilite){
        unhighlight();
        this.origColor=this.style.backgroundColor;
        this.style.backgroundColor='#BCD4EC';
        this.hilite = true;
      }
      else{
        this.style.backgroundColor=this.origColor;
        this.hilite = false;
      }
    }
  }
}

function cloneAllRepositories(){
  console.log("downloadPath: ", config.get('downloadPath'));
  for(var i = 0; i < projects_response.length; i++){
    var repoUrlArray = [];
    repoUrlArray.push("git@csgrad06.nwmissouri.edu:"+semester)
  }
  console.log("");
  repoUrl = 'git@csgrad06.nwmissouri.edu:S2017-44555.NS/Grades_Test.git'
  //folderDestination = document.getElementById("---").value()
  folderDestination = config.get('downloadPath')
  console.log(folderDestination);
  //folderDestination = config.get(downloadPath)
  var clone_response = ipc.sendSync('clone_project', repoUrl, folderDestination,'Grades_Test');
        alert(clone_response);
        //projects_response = ipc.sendSync('load_projects');

        console.log(projects_response)

        for(var i = 0; i < projects_response.length; i++){
        semester = projects_response[i].split_namespace_result.semester
        course = projects_response[i].split_namespace_result.course
        project = projects_response[i].split_namespace_result.project
        section = projects_response[i].split_namespace_result.section
        courseName = projects_response[i].split_namespace_result.courseName

        console.log(semester, course, section, courseName, project)
      }

  if (clone_response) {
    alert("Clone success")
  }
  else {
    alert("Clone is Unsuccessful")
  }
}

function highlight(ctrl){
  if (ctrl.style.background != '#688a7e') {
    ctrl.style.background = '';
  } else {
    ctrl.style.background = 'white';
  }
}

function unhighlight(){
var table = document.getElementById('dataTable');
for (var i=0;i < table.rows.length;i++){
    var row = table.rows[i];
    row.style.backgroundColor=this.origColor;
    row.hilite = false;
}
}
function Delete()
{

};

function removeSection() {
var table = document.getElementById("modifySection");
var par = $(this).parent().parent();
par.remove();
//	var row = table.insertRow(-1);
//	var cell = row.insertCell(-1);
//	cell.innerHTML = "This is a new section.";
alert("A section was modified.");
}

function removeStudent() {
var table = document.getElementById("modifyStudent");
var par = $(this).parent().parent();
par.remove();
//var row = table.insertRow(-1);
//var cell = row.insertCell(-1);
//	cell.innerHTML = "This is a new student."
alert("A student was modified.");
}

function overall(){
var rowCount = document.getElementById('modifySection').rows.length;
document.getElementById('overallSections').innerHTML = (rowCount-1) + " Sections"
}

// window.onload = overall

// $(function () {
// $("td").dblclick(function () {
//   var OriginalContent = $(this).text(); $(this).addClass("cellEditing");
//   $(this).html("<input type='text' value='" + OriginalContent + "' />");
//   $(this).children().first().focus();
//   $(this).children().first().keypress(function (e) {
//     if (e.which == 13) {
//       var newContent = $(this).val();
//       $(this).parent().text(newContent);
//       $(this).parent().removeClass("cellEditing");
//     }
//   });
//   $(this).children().first().blur(function() {
//     $(this).parent().text(OriginalContent);
//     $(this).parent().removeClass("cellEditing");
//   });
// });
// });

function highlight(ctrl){
 if (ctrl.style.background != '#688a7e') {
  ctrl.style.background = '';
 } else {
   ctrl.style.background = 'white';
 }
}

function unhighlight(){
  var table = document.getElementById('dataTable');
  for (var i=0;i < table.rows.length;i++){
    var row = table.rows[i];
    row.style.backgroundColor=this.origColor;
    row.hilite = false;
  }
}

function addSection() {
  var table = document.getElementById("newSection");
  var row = table.insertRow(-1);
  var cell = row.insertCell(-1);
  cell.innerHTML = "new section(Double click to edit).";
  alert("New Section was added!");
}

function addStudent() {
  var table = document.getElementById("newStudent");
  var row = table.insertRow(-1);
  var cell = row.insertCell(-1);
  table.length<=100;
  cell.innerHTML = "new student(Double click to edit)";
  alert("Student was added!");
}
// $("#div table td").dblclick(function(){
//     var sec = $(this);
//     var value = $(this).html();
//     updateVal(sec, value);
// });

//edit section names
function updateVal(sections, value)
{
    $(sections).html('<input class="sections" type="text" value="'+value+'" />');
    $(".sectionsCount").focus();
    $(".sectionsCount").keyup(function(event){
        if(event.keyCode == 13){
            $(currentEle).html($(".thVal").val().trim());
        }
    });

    $('body').not(".sectionsCount").click(function(){
        if(('.thVal').length != 0)
        {
            $(sections).html($(".sectionsCount").val().trim());
        }
    });
}

// window.onload = overall

// $(function () {
//   $("td").dblclick(function () {
//     var OriginalContent = $(this).text(); $(this).addClass("cellEditing");
//     $(this).html("<input type='text' value='" + OriginalContent + "' />");
//     $(this).children().first().focus();
//     $(this).children().first().keypress(function (e) {
//       if (e.which == 13) {
//         var newContent = $(this).val();
//         $(this).parent().text(newContent);
//         $(this).parent().removeClass("cellEditing");
//       }
//     });
//     $(this).children().first().blur(function() {
//       $(this).parent().text(OriginalContent);
//       $(this).parent().removeClass("cellEditing");
//     });
//   });
// });

function logout() {
  document.href = "login.html";
  config.set('loggedIn', false);
  config.set('username',"");
  config.set('profilename',"");
  config.set('private_token',"");
}
